<?php require_once "./code.php";?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>S04: Access Modifiers and Inheritance</title>
</head>

<body>
	<h1>Activity</h1>

    <h2>Building Variables</h2>
    <p><?php echo $building->printName();?></p>
    <p><?php echo $building->printFloors();?></p>
    <p><?php echo $building->printAddress();?></p>

    <?php $building->setName('Caswym Complex'); ?>

    <h2>Condominium Variables</h2>
    <p><?php echo $condominium->printName();?></p>
    <p><?php echo $condominium->printFloors();?></p>
    <p><?php echo $condominium->printAddress();?></p>

    <?php $condominium->setName('Enzo Tower'); ?>

    <p>The name of the condominium has been changed to <?= $condominium->getName(); ?></p>
</body>

