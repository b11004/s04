<?php


class Building {
	protected $name;
	protected $floors;
	protected $address;

	// Constructor is used during the creation of an object.
	public function __construct($name, $floors, $address){
		$this->name = $name;
		$this->floors = $floors;
		$this->address = $address;

	}

    public function printName() {
        return "The name of this building is $this->name";
    }

    public function printFloors() {
        return "The $this->name has $this->floors floors.";
    }

    public function printAddress() {
        return "The $this->name is location at $this->address.";
    }

    public function getName() {
        return $this->$name;
    }

    public function setName($name){
        $this->name = $name;
    }
}

class Condominium extends Building{

    public function printName() {
        return "The name of this building is $this->name";
    }

    public function printFloors() {
        return "The $this->name has $this->floors floors.";
    }

    public function printAddress() {
        return "The $this->name is location at $this->address.";
    }
	// Encapsulation - getter
	public function getName(){
		return $this->name;
	}

	// Encapsulation - setter
	public function setName($name){
		$this->name = $name;
	}
}


$building = new Building('Caswynn Building', 8, 'Timog Ave., Quezon City, Philippines');
$condominium = new Condominium('Enzo Condo' , '5' , 'Buendia Ave, Makati City, Philippines');
