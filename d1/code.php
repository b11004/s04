<?php 

/* 
    3 Types of Access Modifiers

    1. public - the property or method can be accessed from everywhere. This is default. 
    
    2. protected - the property or method can be accessed within the class and by classes derived from that class
    
    3. private - the property or method can ONLY be accessed within the class.

*/

class Building {
	// Access Modifier (Public)
	protected $name;
	protected $floors;
	protected $address;

	public function __construct($name, $floors, $address) {
		$this->name = $name;
		$this->floors = $floors;
		$this->address = $address;
	}
}

class Condominium extends Building {
	// Encapsulation - Getter
	public function getName() {
		return $this->name;
	}

	// Encapsulation - Setter
	public function setName($name) {
		$this->name = $name;
	}
}

$building = new Building('Caswynn Building', 8, 'Timog Avenue', 'Quezon City', 'Philippines');

$condominium = new Condominium('Enzo Condo', 5, 'Buendia Avenue, Makati City', 'Philippines');